<?php

/**
 * @file
 * Contains drush commands to import environment config.
 *
 * @author: Matt Gill <http://drupal.org/u/mattgill>
 */

/**
 * Implements hook_drush_command().
 */
function environment_config_drush_command() {
  $items = [];
  $items['config-import'] = [
    'description' => 'Import environment specific config',
    'arguments' => [
      'directory' => 'The config directory to import',
    ],
    'drupal dependencies' => ['environment_config'],
    'aliases' => ['environment_config_import'],
  ];
  return $items;
}

/**
 * Implements drush_MODULE_ID_COMMAND_ID().
 *
 * Posts webform metadata to storage engine when triggered by drush.
 *
 * @param string $directory
 *   The directory to import config form.
 */
function drush_environment_config_config_import($directory = '') {
  if ($directory == '') {
    drush_print('No configuration directory specified');
    return;
  }
  drush_print('Importing from ' . $directory);
  if (environment_config_do_import($directory)) {
    drush_print('Import complete! Remember to rebuild cache for changes to take effect.');
  }
  else {
    drush_print('Import failed. Check directory paths, Drupal logs and try again.');
  }
}
